import { motion } from 'framer-motion';
import React from 'react';
import { Redirect, RedirectProps } from 'react-router-dom';

export const MotionRedirect: React.FC<RedirectProps> = props => (
  <motion.div exit="undefined">
    <Redirect {...props} />
  </motion.div>
);
