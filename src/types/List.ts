import { Task } from './Task';

export type List = {
  id: string;
  name: string;
  creationDate: Date;
  archived: boolean;
  color: string;
  tasks: Task[];
};
