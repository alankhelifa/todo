export type Task = {
  id: string;
  subject: string;
  description?: string;
  creationDate: Date;
  deadline?: Date;
  done: boolean;
};
