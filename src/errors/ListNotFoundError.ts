export class ListNotFoundError extends Error {
  private readonly _id: string;

  constructor(id: string, message?: string) {
    super(message);
    this._id = id;
    this.name = 'ListNotFoundError';
  }

  get id(): string {
    return this._id;
  }
}
