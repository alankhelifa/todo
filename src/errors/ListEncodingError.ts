export class ListEncodingError extends Error {
  constructor(message?: string) {
    super(message);
    this.name = 'ListEncodingError';
  }
}
