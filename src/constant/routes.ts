export const ROUTES = {
  LIST: (id = ':id'): string => `/lists/${id}`,
  LISTS: (): string => '/lists',
  TASK: (listId = ':listId', id = ':id'): string => `/lists/${listId}/${id}`,
  SETTINGS: (): string => '/settings',
};
