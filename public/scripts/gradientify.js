/*
MIT License

Copyright (c) 2018 Karol Świerczek

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

"use strict";!function(){var t=[];function e(){return void 0===this||Object.getPrototypeOf(this)!==e.prototype?new e:this}function n(t,e){t.map(function(t){e.appendChild(t)})}function o(t,e,n){return t.map(function(t,o){var i=document.createElement("div");return Object.assign(i.style,{backgroundImage:t,opacity:0===o?1:0,transitionDuration:e/1e3+"s",zIndex:n===document.body?-999:2}),i.classList.add("gradientify-gradient"),i})}function i(t,e){setInterval(function(){for(var e=0;e<t.length;e++)"1"===t[e].style.opacity&&(t[e].style.opacity=0,t[++e%t.length].style.opacity=1)},e+200)}e.prototype.create=function(t,e,a){var r;n(r=o(e,a,t),t),i(r,a)},e.prototype.loadFromPreset=function(e,a,r){var s,c=void 0;t.map(function(t){t.hash===a&&(r=t.interval,c=t.gradients)}),n(s=o(c,r,e),e),i(s,r)},e.prototype.getPresets=function(e,n){var o=new XMLHttpRequest;o.overrideMimeType("application/json"),o.open("GET",e,!0),o.onreadystatechange=function(){4===o.readyState&&200===o.status&&(t=JSON.parse(o.responseText),n())},o.send(null)},window.Gradientify=e}();